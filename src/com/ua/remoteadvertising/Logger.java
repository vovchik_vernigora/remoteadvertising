package com.ua.remoteadvertising;

import android.widget.Toast;

public class Logger {

	public enum LogTo { Toast, Debug, Null};
	
	private static LogTo Current = LogTo.Toast;
	
	public static void Log(String str)
	{
		Print(str);
	}
	public static void Log(String str, LogTo logTo)
	{
		Print(str, logTo);
	}
	
	public static void Log(int value)
	{
		Print(value+"");
	}

	public static void Log(int value, LogTo logTo)
	{
		Print(value+"", logTo);
	}
	
	public static void Log(float value)
	{
		Print(value+"");
	}
	
	public static void Log(float value, LogTo logTo)
	{
		Print(value+"", logTo);
	}
	
	public static void Log(Object obj)
	{
		Print(obj.toString());
	}
	
	public static void Log(Object obj, LogTo logTo)
	{
		Print(obj.toString(), logTo);
	}
	
	static void Print(String str, LogTo logTo)
	{
		switch(logTo)
		{
			case Toast :
				Toast.makeText(MainActivity.myCnt, str, Toast.LENGTH_SHORT).show();
				break;
			case Debug : 
				System.out.println(str);
				break;
			case Null : break;
			default : System.out.println(str);
				break;
		}
	}
	
	static void Print(String str)
	{
		switch(Current)
		{
			case Toast :
				Toast.makeText(MainActivity.myCnt, str, Toast.LENGTH_SHORT).show();
				break;
			case Debug : 
				System.out.println(str);
				break;
			case Null : break;
			default : System.out.println(str);
				break;
		}
	}
}
