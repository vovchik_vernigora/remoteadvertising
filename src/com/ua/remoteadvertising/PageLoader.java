package com.ua.remoteadvertising;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.os.AsyncTask;
import android.util.Log;

public abstract class PageLoader extends AsyncTask<String, Integer, String> {
    
    private final String SPLITTER = "!!!";
    
    protected String doInBackground(String... urls) {

        StringBuilder builder = new StringBuilder();
        HttpClient client = new DefaultHttpClient();
        HttpGet httpGet = new HttpGet(urls[0]);
        
        try {
                HttpResponse response = client.execute(httpGet);
                StatusLine statusLine = response.getStatusLine();
                int statusCode = statusLine.getStatusCode();
                if (statusCode == 200) {
                        HttpEntity entity = response.getEntity();
                        InputStream content = entity.getContent();
                        BufferedReader reader = new BufferedReader(
                                        new InputStreamReader(content));
                        String line;
                        while ((line = reader.readLine()) != null) {
                                builder.append(line);
                        }
                        Log.v("Getter", "Your data: " + builder.toString()); //response data
                } else {
                        Log.e("Getter", "Failed to download file");
                }
        } catch (ClientProtocolException e) {
                e.printStackTrace();
        } catch (IOException e) {
                e.printStackTrace();
        }

    return builder.toString();
}
    
    protected void onProgressUpdate(Integer... progress) {
        Log.d(VideoFragment.TAG, progress[0].toString());
    }


    protected void onPostExecute(String result) {
        Log.d(VideoFragment.TAG,"started!");
        String json = result.substring(result.indexOf(SPLITTER));
        json = json.substring(3, json.lastIndexOf(SPLITTER));
        json = json.replace("&quot;", "\"");
        Log.d(VideoFragment.TAG,json);
        done(json);
    }

    protected abstract void done(String json);
}
