package com.ua.remoteadvertising;

public class VideoEntity {
    
    public String URL;
    public int count;
    
    public VideoEntity(String URL, int count)
    {
        this.URL = URL;
        this.count = count;
    }
    
    public VideoEntity(String URL, String count)
    {
        this.URL = URL;
        try {
            this.count = Integer.parseInt(count);
        } catch (NumberFormatException e) {
            this.count = 0;
        }
    }
}
