package com.ua.remoteadvertising;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

import android.os.AsyncTask;

abstract class Loader extends AsyncTask<String, Void, Void> {
    
    private File FULL_DIR_LINK;
    
    public Loader(File FULL_DIR_LINK)
    {
        this.FULL_DIR_LINK = FULL_DIR_LINK;
    }
    
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }
    
    @Override
    protected Void doInBackground(String... params) {
        try {
            
            URL url = new URL(params[0]); //you can write here any link

            URLConnection ucon = url.openConnection();

            System.out.println("content type: " + ucon.getContentType());
            String fileName = ucon.getURL().getFile().substring(ucon.getURL().getFile().lastIndexOf("/") + 1).replaceAll("%20", "_");
            if(!(new File(FULL_DIR_LINK, fileName).exists())){
	            File file = new File(FULL_DIR_LINK + "/loading/", fileName);
	            
	            if (!file.exists() || file.length() != ucon.getContentLength()){
	                int fileLength = ucon.getContentLength();
	                
	                InputStream is = ucon.getInputStream();
	                FileOutputStream fos = new FileOutputStream(file);
	
	                byte data[] = new byte[8192];
	                long total = 0;
	                int count;
	                while ((count = is.read(data)) != -1) {
	                    total += count;
	                    fos.write(data, 0, count);
	                }
	                
	                fos.close();
	                file.renameTo(new File(FULL_DIR_LINK, fileName));
	                Logger.Log(fileName + " loaded");
	            }
            }
            else 
            {
            	 System.out.println(fileName + " exists");
            }
        } catch (IOException e) {
            System.out.println("Error: " + e);
        }
        
        return null;
    }

    @Override
    protected void onPostExecute(Void result) {
        done();
    }
    
    public abstract void done();
  }
