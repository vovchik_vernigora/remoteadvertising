package com.ua.remoteadvertising;

import java.io.File;
import java.io.FilenameFilter;
import java.util.LinkedList;
import java.util.Queue;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Fragment;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Bundle;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.MediaController;
import android.widget.VideoView;

public class VideoFragment extends Fragment {

    private final String LINK = "https://docs.google.com/document/d/1sTSpaIZSTT7V688FsaR8qVVH49ad4kBjg9lfcTUPo9U/pub";
    public static final String TAG = "MyActivity";
    private File FULL_DIR_LINK = new File(Environment.getExternalStorageDirectory(), "/AdvFideoShower/");
    
    private Queue<VideoEntity> videos;
    private Queue<String> videoNamesToShow;
    private Queue<String> deleteList;
    private int videoIndex = 0;
    private Loader loader;
    private VideoView videoView;
    
    private boolean haveInet;
    
    public VideoFragment(boolean haveInet){
        this.haveInet = haveInet;
    }
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container,
                false);
        
        videos = new LinkedList<VideoEntity>();
        videoNamesToShow = new LinkedList<String>();
        deleteList = new LinkedList<String>();
        //FULL_DIR_LINK = MainActivity.myCnt.getFilesDir();
        return rootView;
    }
    
    @Override
    public void onActivityCreated(Bundle savedInstanceStat){
        super.onActivityCreated(savedInstanceStat);
        
        videoView =(VideoView)getView().findViewById(R.id.videoView);
        
        MediaController mediaController = new MediaController(getActivity());
        mediaController.setAnchorView(videoView);
        videoView.setMediaController(mediaController);
        
        videoView.setOnCompletionListener( new OnCompletionListener() {
            public void onCompletion(MediaPlayer mp) {
                mp.stop();
                if(deleteList.size() > 0)
                {
                    deleteFile(deleteList.poll());
                }
                setupVideoView();
            }
        });
        
        // instantiate it within the onCreate method
        createDirIfNotExists(FULL_DIR_LINK);
        
        if(haveInet){
            loadJSON();
        }
        else {
            setupVideoView();
        }
    }
    
    
    private void parseJson(String json)
    {
        JSONObject obj;
        try {
        obj = new JSONObject(json);
         JSONArray arr = obj.getJSONArray("files");
        for (int i = 0; i < arr.length(); i++)
        {
            String url = arr.getJSONObject(i).getString("URL");
            String count = arr.getJSONObject(i).getString("count");
            String name = arr.getJSONObject(i).getString("name");
            name = name.replace(' ', '_');
            Logger.Log(" + added file " + name);
            videoNamesToShow.add(name);
            videos.add(new VideoEntity(url, count));
        }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (videos.size() > 0)
        {
            startLoadingVideos();
        }
    }
    
    private void startLoadingVideos()
    {
        VideoEntity entity = videos.poll();
        if(entity != null){
            loader = new Loader(FULL_DIR_LINK){
                @Override
                public void done() {
                    if(!videos.isEmpty()){
                        this.cancel(true);
                        startLoadingVideos();
                        
                        if(!videoView.isPlaying())
                        {
                            setupVideoView();
                        }
                    }
                    else {
                        deleteUnusededVideos();
                    }
                }
            };
            loader.execute(entity.URL);
        }
    }
    
    void deleteUnusededVideos()
    {
        String[] files  = getFiles();
        for(String file : files)
        {
            if(!videoNamesToShow.contains(file))
            {
                deleteFile(file);
            }
        }
    }
    
    void deleteFile(String filename)
    {
        if(!currentPlayedVideo.substring(currentPlayedVideo.lastIndexOf('/')+1).equals(filename)){
            Logger.Log(" - delete file " + filename);
            Logger.Log(" - file deleted = " + new File(FULL_DIR_LINK+"/" + filename).delete());
        }
        else
        {
        	Logger.Log(" tryed to delete file. now in pull " + filename);
            deleteList.add(filename);
        }
    }
    
    public void loadJSON() {
        PageLoader task = new PageLoader(){
            @Override
            protected void done(String json) {
                parseJson(json);
            }
        };
        task.execute(new String[] { LINK });
    }
    
    public static boolean createDirIfNotExists(File _file) {
        boolean ret = true;
        
        Boolean isSDPresent = Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED);
        if(!isSDPresent) {
        	Logger.Log("no SD card");
        }

        File file = new File(_file + "/loading/");
        if (!file.exists()) {
            if (!file.mkdirs()) {
                ret = false;
            }
        }
        return ret;
    }
    
    FilenameFilter textFilter = new FilenameFilter() {
        public boolean accept(File dir, String name) {
            String lowercaseName = name.toLowerCase();
            if (lowercaseName.endsWith(".mp4") || lowercaseName.endsWith(".avi")) {
                return true;
            } else {
                return false;
            }
        }
    };
    
    public String[] getFiles(){
        return FULL_DIR_LINK.list(textFilter);
    }
    
    public String getNewxtVideo(){
        String[] files  = getFiles();
        
        String linkForNextFile = null;
        
        if(files != null && files.length > 1)
        {
            linkForNextFile = FULL_DIR_LINK.toString() +"/"+ files[videoIndex++];
            if (videoIndex >= files.length) {
                videoIndex = 0;
            }
        }
        
        return linkForNextFile;
    }
    
    String currentPlayedVideo;
    public void setupVideoView(){
        currentPlayedVideo = getNewxtVideo();
        if(currentPlayedVideo != null){
            Logger.Log("playing " + currentPlayedVideo);
            videoView.setVideoPath(currentPlayedVideo);
            videoView.start();
        } 
    }
}
