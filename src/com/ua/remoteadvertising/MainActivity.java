package com.ua.remoteadvertising;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends Activity {
    
    public static Context myCnt; 
    
    //private ProgressDialog mProgressDialog;
    private View decorView;
    private int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                          | View.STATUS_BAR_HIDDEN;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        /*mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage("Loading file...");
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        mProgressDialog.setCancelable(true);
        */
        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                .add(R.id.container, new VideoFragment(isNetworkConnected())).commit();
        }
        decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(uiOptions);
        
        myCnt = this;
        
        TextView textView = (TextView)findViewById(R.id.bottomText);
        textView.setText("Вова, пишемо любий текст!!!Вова, пишемо любий текст!!!Вова, пишемо любий текст!!!Вова, пишемо любий текст!!!Вова, пишемо любий текст!!!Вова, пишемо любий текст!!!");
        textView.setSelected(true);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        if (ni == null) {
         // There are no active networks.
         return false;
        } else
         return true;
    }
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public void onWindowFocusChanged(boolean visible)
	{
       decorView.setSystemUiVisibility(uiOptions);
	}
}
